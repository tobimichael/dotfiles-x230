X230 setup
------------------
* Notification `dunst`
* Font `Roboto Mono`
* Terminal Emulator `xterm`
* Bar `polybar`
* Window Manager `bspwm`
